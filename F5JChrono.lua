-- ***************************************************************************** 
-- * F5J Chrono
-- *
-- * Copyright (c) 2016 - 2019, Thierry WILMOT
-- * All rights reserved.
-- *
-- * Redistribution and use in source and binary forms, with or without
-- * modification, are permitted provided that the following conditions are met:
-- * 
-- * 1. Redistributions of source code must retain the above copyright notice, this
-- *    list of conditions and the following disclaimer.
-- * 2. Redistributions in binary form must reproduce the above copyright notice,
-- *    this list of conditions and the following disclaimer in the documentation
-- *    and/or other materials provided with the distribution.
-- * 
-- * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
-- * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
-- * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
-- * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
-- * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
-- * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
-- * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
-- * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
-- * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- * 
-- * The views and conclusions contained in the software and documentation are those
-- * of the authors and should not be interpreted as representing official policies,
-- * either expressed or implied, of the FreeBSD Project.                    
-- *    
-- *    
-- * v 1.0 - initial release   
-- * v 2.0 - add alti, motorCount & historic
-- * v 2.1 - add timer in millisec
-- * v 2.2 - remove color function for DC16
-- * v 2.3 - remove historic for DC16, add training work duration for DC24
-- *****************************************************************************

-- Timers variables
local workDuration = 10*60000					-- 10 min
local flyoffExtraDuration = 5*60000		-- 5 min
local preparDuration = 10000					-- 10 sec
local lastTime, lastTimeSec = 0, 0
local remainPreparTime, remainWorkingTime, flyTime, motorTime, remainBeforeAlt

local altiMotorStop, motorCount = 0, 0
local getHeightTime = 15000					-- Display altitude after 15 sec of fligth time

-- Misc variables
local lang, jsnConfig
local rootDir = "/Apps/F5JChrono"
local audioDir
local flyoffCoef = 0						      -- Multiplier coef
local wrkTimeSwitch, stopSwitch, motorSwitch, fastSwitch, statusSwitch, altiSensorIdx

local motorNeverStarted
local lastWrkVal, lastFlyVal, lastStopVal, lastMotorVal, lastFastVal, lastDurVal, lastStatusVal
#ifdef DX24
local preparColor, workColor, flightColor, motorColor, altColor = 0, 0, 0, 0, 0
local historicList, showHistoric, yHistoric = {}, false, 0
local trainingDuration = 0
#endif
local altiSensorList = {}
local currentAltiSensorID, currentAltiSensorParam

-- State machine
local ST_WAIT_START = 0
local ST_PREPAR     = 1
local ST_WORK       = 2
local ST_MOTOR      = 3
local ST_BEFORE_ALT = 4
local ST_FLIGHT     = 5
local ST_STOP       = 6
local ST_OVERTIME   = 7
local ST_RESET      = 8
local currentState = ST_WAIT_START


--------------------------------------------------------------------
-- Misc functions
--------------------------------------------------------------------

local function timeStr(value)
   local neg = ""
   
   -- because lua modulo is buggy with negative numbers
   if(value < 0) then
      neg = "-"
      value = -value 
   end
   
   return neg..string.format("%.02d:%.02d.%.1d",
         value/60000,
         value/1000%60,
         value/100%10)
end

#ifdef DX24
local function setColor(color)
   lcd.setColor((color&0xFF0000)>>16,(color&0x00FF00)>>8,color&0x0000FF)
end
#endif

local function getSensorID(label)
	local sensors = system.getSensors()
	local t = {}
	for _,sensor in ipairs(sensors) do
		if(sensor.param == 0) then
			t[sensor.id] = sensor.label
		end
	end

	for _,sensor in ipairs(sensors) do
		if(t[sensor.id] and sensor.param ~= 0) then
			if(t[sensor.id].."."..sensor.label == label) then
				return sensor.id, sensor.param
      	end
		end
	end

   return nil, nil
end

local function updateTimeVariables()   
   if(flyoffCoef == 0) then
      flyoffExtraDuration = 0
   elseif(flyoffCoef == 1) then
      flyoffExtraDuration = 5*60000
#ifdef DX24
   else
      flyoffExtraDuration = trainingDuration*60000
#endif
   end
   
   remainWorkingTime = workDuration + flyoffCoef*flyoffExtraDuration
   remainPreparTime = preparDuration

   flyTime = 0
   motorTime = 0
   remainBeforeAlt = 10000   -- 10 sec after motor cut altimeter notification
   motorNeverStarted = true
	altiMotorStop = 0
end

local function updateTimeVariablesAndSave()
   updateTimeVariables()

   system.pSave("flyoffCoef", flyoffCoef)
   system.pSave("preparDuration", preparDuration)
   system.pSave("wrkTimeSwitch", wrkTimeSwitch)
   system.pSave("stopSwitch", stopSwitch)
   system.pSave("motorSwitch", motorSwitch)
   system.pSave("fastSwitch", fastSwitch)
   system.pSave("statusSwitch", statusSwitch)
#ifdef DX24
   system.pSave("motorCount", motorCount)
   system.pSave("trainDuration", trainingDuration)
#endif
end

#ifdef DX24
local function saveHisto()
	local txt = json.encode(historicList)
	local f = io.open(rootDir.."/histo.jsn","w")
	if(f) then
		io.write(f, txt, "\n")
		io.close(f)
	end
	txt = nil
	f = nil
	collectgarbage()
end
#endif

local function initVariables()
   currentState = ST_WAIT_START
   updateTimeVariables()
   lastTime = system.getTime()
end

local function onStopChrono()
#ifdef DX24
	if(#historicList <= jsnConfig.maxHistoScore) then
		local str = string.format("%.2d        %.2d:%.2d.%.1d      %.2d:%.2d.%.1d      %.3d", #historicList+1, flyTime/60000, flyTime/1000%60, flyTime/100%10, motorTime/60000, motorTime/1000%60, motorTime/100%10, altiMotorStop)
		historicList[#historicList+1] = str
		saveHisto()
	end
#endif

	initVariables()
	-- currentState = ST_WAIT_START
	form.reinit()
end

--------------------------------------------------------------------
-- Read configuration
--------------------------------------------------------------------

local function readConfig()
	local lng = system.getLocale()
	local file = io.readall(rootDir.."/locale.jsn")
	local obj = json.decode(file)
	if(obj) then
		lang = obj[lng] or obj[obj.default]
		obj = nil
	end

#ifdef DX24
	file = io.readall(rootDir.."/histo.jsn")
	historicList = json.decode(file or "[]")
#endif

   -- Read timer config file (compatible format with Jeti one)
   file = io.readall(rootDir.."/config.jsn")
   jsnConfig = json.decode(file).config
	file = nil

   -- Load main config from persistent parameters
   flyoffCoef = system.pLoad("flyoffCoef", 0)						-- default 'regular round'
   preparDuration = system.pLoad("preparDuration", 10000)	-- default 10 sec
   wrkTimeSwitch = system.pLoad("wrkTimeSwitch")
   stopSwitch = system.pLoad("stopSwitch")
   motorSwitch = system.pLoad("motorSwitch")
   fastSwitch = system.pLoad("fastSwitch")
   statusSwitch = system.pLoad("statusSwitch")
   altiSensorIdx = system.pLoad("altiSensorIdx", 1)
#ifdef DX24
   motorCount = system.pLoad("motorCount", 0)
   trainingDuration = system.pLoad("trainDuration", 0)
#endif

   if(jsnConfig.audioDir) then
      audioDir = jsnConfig.audioDir..lng.."/"
   else
     audioDir = rootDir.."/audio/"..lng.."/"
   end

	if(jsnConfig.getHeightTime) then
		getHeightTime = jsnConfig.getHeightTime*1000
	end

	-- Find an altitude/height sensor
	local sensors = system.getSensors()
	table.insert(altiSensorList, "...")
	
	-- get device name
	local t = {}
	for _,sensor in ipairs(sensors) do
		if(sensor.param == 0) then
			t[sensor.id] = sensor.label
		end
	end
	-- get device fields
	for _,sensor in ipairs(sensors) do
		if(t[sensor.id] and sensor.param ~= 0) then
			table.insert(altiSensorList, t[sensor.id].."."..sensor.label)
		end
	end

	sensors = nil
	currentAltiSensorID, currentAltiSensorParam = getSensorID(altiSensorList[altiSensorIdx])

	collectgarbage()
end

--------------------------------------------------------------------
-- Telemetry functions
--------------------------------------------------------------------

local function printTelemetry()
#ifdef DX24
	lcd.setColor(0, 0, 0)

	if(showHistoric) then
		local x, y = 10, -yHistoric*lcd.getTextHeight(FONT_NORMAL)*6
		lcd.drawText(x, y, lang.hhl, FONT_BOLD)
		y = y + lcd.getTextHeight(FONT_BOLD) + 2
		x = 30

		for i=1, #historicList do
			lcd.drawText(x, y, historicList[i], FONT_NORMAL)
			y = y + lcd.getTextHeight(FONT_NORMAL)
		end
	else
		if(flyoffCoef == 0) then
			lcd.drawText(5, 10, lang.rrl.." - "..string.format(lang.mclt, motorCount))
		elseif(flyoffCoef == 1) then
			lcd.drawText(5, 10, lang.frl.." - "..string.format(lang.mclt, motorCount))
      else
      	lcd.drawText(5, 10, lang.trl.." - "..string.format(lang.mclt, motorCount))
      end
#endif
#ifdef DX16
		if(flyoffCoef == 0) then
			lcd.drawText(5, 10, lang.rrl)
		elseif(flyoffCoef == 1) then
			lcd.drawText(5, 10, lang.frl)
      else
			lcd.drawText(5, 10, lang.trl)      
      end
#endif
		if(currentState == ST_WAIT_START) then
			if(system.getTime() % 2 == 0) then		-- make text blink
				lcd.drawText(190, 10, lang.r2sl)
			end

#ifdef DX24
			setColor(preparColor)
#endif
			lcd.drawText(50, 40, string.format("Prepar. %.2d:%.2d",remainPreparTime/60000,remainPreparTime/1000%60), FONT_MAXI)
		elseif(currentState == ST_PREPAR) then
#ifdef DX24
			setColor(preparColor)
#endif
			lcd.drawText(50, 40, string.format("Prepar. %.2d:%.2d",remainPreparTime/60000,remainPreparTime/1000%60), FONT_MAXI)
		elseif(currentState == ST_WORK) then
#ifdef DX24
			setColor(workColor)
#endif
--			lcd.drawText(50, 40, string.format(lang.wl.."   %.2d:%.2d.%.1d",remainWorkingTime/60000,remainWorkingTime/1000%60,remainWorkingTime/100%10), FONT_MAXI)
			lcd.drawText(50, 40, string.format(lang.wl.."   "..timeStr(remainWorkingTime)), FONT_MAXI)
		elseif((currentState == ST_MOTOR) or 
		       (currentState == ST_BEFORE_ALT) or 
		       (currentState == ST_FLIGHT) or 
		       (currentState == ST_STOP) or
		       (currentState == ST_OVERTIME)
		       ) then
#ifdef DX24
			setColor(workColor)
#endif
			lcd.drawText(40, 30 , lang.wl, FONT_MAXI)
--			lcd.drawText(160, 30 , string.format("%.2d:%.2d.%.1d",remainWorkingTime/60000,remainWorkingTime/1000%60,remainWorkingTime/100%10), FONT_MAXI)
			lcd.drawText(160, 30, string.format(timeStr(remainWorkingTime)), FONT_MAXI)
#ifdef DX24			
			setColor(flightColor)
#endif
			lcd.drawText(40, 65 , lang.fl, FONT_MAXI)
--			lcd.drawText(160, 65 , string.format("%.2d:%.2d.%.1d",flyTime/60000,flyTime/1000%60,flyTime/100%10), FONT_MAXI)
			lcd.drawText(160, 65, string.format(timeStr(flyTime)), FONT_MAXI)
#ifdef DX24			
			setColor(motorColor)
#endif
			lcd.drawText(40, 100 , lang.ml, FONT_MAXI)
--			lcd.drawText(160, 100, string.format("%.2d:%.2d.%.1d",motorTime/60000,motorTime/1000%60,motorTime/100%10), FONT_MAXI)
			lcd.drawText(160, 100, string.format(timeStr(motorTime)), FONT_MAXI)

			if(altiMotorStop ~= 0) then
				lcd.drawText(250, 132, string.format("H %.3d m",altiMotorStop), FONT_NORMAL)			
			end			
		end
		
		if(currentState == ST_BEFORE_ALT) then
#ifdef DX24
			setColor(altColor)
#endif
			lcd.drawText(270, 105, string.format("Alt %.2d",remainBeforeAlt/1000), FONT_NORMAL)
		elseif(currentState == ST_STOP) then
			lcd.drawText(190, 10, lang.sl)
		elseif(currentState == ST_OVERTIME) then
   		lcd.drawText(190, 10, lang.ovt)
		end

#ifdef DX24
		lcd.setColor(0, 0, 0)
		lcd.drawText(0, 145, "(c) 2017-2018 QuarkEmu - v2.3 - DX24", FONT_MINI)
#endif
#ifdef DX16
		lcd.drawText(0, 145, "(c) 2017-2018 QuarkEmu - v2.3 - DX16", FONT_MINI)
#endif
#ifdef DX24
	end
#endif
end

--------------------------------------------------------------------
-- form functions
--------------------------------------------------------------------

local function onRoundDurSelBoxChange(value)
   if(value == 1) then
      flyoffCoef = 0
   elseif(value == 2) then
      flyoffCoef = 1
   else
      flyoffCoef = -1
   end
   updateTimeVariablesAndSave()
   form.reinit()
end

#ifdef DX24
local function onTraimingDurationChange(value)
   trainingDuration = 10 - value
   updateTimeVariablesAndSave()
end
#endif

local function onPreFlightDurSelBoxChange(value)
   preparDuration = value*1000
   updateTimeVariablesAndSave()
end

local function onSwitchWrkChange(value)
	wrkTimeSwitch = value
   updateTimeVariablesAndSave()
end

local function onSwitchStopChange(value)
	stopSwitch = value
   updateTimeVariablesAndSave()
end

local function onSwitchMotorChange(value)
	motorSwitch = value
   updateTimeVariablesAndSave()
end

local function onSwitchFastPreparChange(value)
	fastSwitch = value
   updateTimeVariablesAndSave()
end

local function onSwitchStatusChange(value)
	statusSwitch = value
   updateTimeVariablesAndSave()
end

local function onAltiChange(value)
	altiSensorIdx = value
	currentAltiSensorID, currentAltiSensorParam = getSensorID(altiSensorList[altiSensorIdx])
   system.pSave("altiSensorIdx", altiSensorIdx)
end

#ifdef DX24
local function onMotorCoutChange(value)
	motorCount = value
   updateTimeVariablesAndSave()
end

local function onClearHistoChange(value)
	for i = value, #historicList do
		table.remove(historicList, i)
	end
	saveHisto()
end
#endif

local function initForm() 
   form.addRow(2)
   form.addLabel({label=lang.rdl})

#ifdef DX24
   local idx
   if(flyoffCoef == 0) then
      idx = 1
   elseif(flyoffCoef == 1) then
      idx = 2
   else
      idx = 3
   end
   form.addSelectbox({lang.rrl,lang.frl,lang.trl}, idx, true, onRoundDurSelBoxChange)

   -- if training mode
   if(flyoffCoef < 0) then
      form.addRow(2)
      form.addLabel({label=lang.twt})
      form.addIntbox(10-trainingDuration, 1, 10, 0, 0, 1, onTraimingDurationChange, {label=" Min"})
   end
#endif
#ifdef DX16
   form.addSelectbox({lang.rrl,lang.frl}, flyoffCoef+1, true, onRoundDurSelBoxChange)
#endif

   form.addRow(2)
   form.addLabel({label=lang.pfdl, width=210})
   form.addIntbox(preparDuration/1000, 0, 120, 0, 0, 1, onPreFlightDurSelBoxChange, {label=" Sec"})

   form.addRow(2)
   form.addLabel({label=lang.wtsl, width=210})
   form.addInputbox(wrkTimeSwitch, true, onSwitchWrkChange)

   form.addRow(2)
   form.addLabel({label=lang.msl, width=210})
   form.addInputbox(motorSwitch, true, onSwitchMotorChange)

   form.addRow(2)
   form.addLabel({label=lang.srsl, width=210})
   form.addInputbox(stopSwitch, true, onSwitchStopChange)

   form.addRow(2)
   form.addLabel({label=lang.fpsl, width=210})
   form.addInputbox(fastSwitch, true, onSwitchFastPreparChange)

   form.addRow(2)
   form.addLabel({label=lang.ssl, width=210})
   form.addInputbox(statusSwitch, true, onSwitchStatusChange)

   form.addRow(2)
   form.addLabel({label=lang.al, width=210})
   form.addSelectbox(altiSensorList, altiSensorIdx, true, onAltiChange)

#ifdef DX24
   form.addRow(2)
   form.addLabel({label=lang.mcl, width=210})
   form.addIntbox(motorCount, 0, motorCount, 0, 0, 1, onMotorCoutChange)

   form.addRow(2)
   form.addLabel({label=lang.chl, width=210})
   form.addIntbox(#historicList+1, 1, #historicList+1, 1, 0, 1, onClearHistoChange)
#endif
end

-----------------------------------------------------------------------
-- Main init function
-- 
-----------------------------------------------------------------------

local function mainInit()
   readConfig()
   initVariables()

   system.registerTelemetry(1, lang.an, 4, printTelemetry)
   system.registerForm(1, MENU_TELEMETRY, lang.an, initForm, nil, nil)
end

-----------------------------------------------------------------------
-- Start action if time is matching
-- 
-----------------------------------------------------------------------

local function startAction(jConf, expectVal)
	local ret

	if(jConf) then
		for j = 1, #jConf do
			if(jConf[j].Time == expectVal//1000) then
  
				-- looking for exclusive sound action
				if(jConf[j].File) then
					system.playFile(audioDir..jConf[j].File, AUDIO_QUEUE)
				elseif(jConf[j].Cnt) then
					system.playBeep(jConf[j].Cnt, jConf[j].Freq, jConf[j].Length)
				elseif(jConf[j].Number) then
					system.playNumber(jConf[j].Number, jConf[j].Decimals, jConf[j].Unit, jConf[j].Label)
				end

#ifdef DX24
				if(jConf[j].Vib) then
					local leftRight = false
					if(string.sub(jConf[j].Vib, 1, 1) == "R") then
						leftRight = true
					end
					local vibProf = string.format("%d", string.sub(jConf[j].Vib, 2, 2))
					system.vibration(leftRight, vibProf)
				end

				-- loocking for color action
				if(jConf[j].Color) then
   				ret = jConf[j].Color
				end
#endif
				break
			end
		end
	end

	return ret
end

-----------------------------------------------------------------------
-- Main loop function
-- Warning: All code put in this function must be carrefully optimized
-----------------------------------------------------------------------

local function mainLoop()
   -- Check if input controls changes
   local wrkVal, stopVal, motorVal, statusVal = system.getInputsVal(wrkTimeSwitch, stopSwitch, motorSwitch, statusSwitch)

   -- Optimize: dont compute anything until 3 switches are not setting up
   if((wrkVal == nil) or (stopVal == nil) or (motorVal == nil)) then
      return
   end

   -- Get first values
   if(lastWrkVal == nil) then
      lastWrkVal = wrkVal
   end
   if(lastStopVal == nil) then
      lastStopVal = stopVal
   end
   if(lastMotorVal == nil) then
      lastMotorVal = motorVal
   end
   if(lastStatusVal == nil) then
      lastStatusVal = statusVal
   end

   -- fast selection usage
	if(fastSwitch) then
		if(currentState == ST_WAIT_START) then
			local fastVal = system.getInputsVal(fastSwitch)
#ifdef DX24
			if(showHistoric == true) then									-- input is use as a scroller
				if(lastFastVal) then
					yHistoric = yHistoric + (lastFastVal - fastVal)
				end
				lastFastVal = fastVal
			else
#endif
				for i = 1, #jsnConfig.fastSel do
					if(fastVal < jsnConfig.fastSel[i].Track) then
						preparDuration = jsnConfig.fastSel[i].Time*1000
						if(preparDuration ~= lastDurVal) then
							remainPreparTime = preparDuration
							updateTimeVariablesAndSave()

							-- check if audio file present
							if(jsnConfig.fastSel[i].File) then
								system.playFile(audioDir..jsnConfig.fastSel[i].File, AUDIO_QUEUE)
							end

							lastDurVal = preparDuration
						end
						break
					end
#ifdef DX24
				end
#endif
			end
		end
	end

	-- On raising hedge of status switch
	if(statusSwitch and ((statusVal - lastStatusVal) > 0)) then
		if((currentState == ST_WORK) or (currentState == ST_FLIGHT)) then
			system.playNumber(remainWorkingTime/60000, 0, "min")
			system.playNumber(remainWorkingTime/1000%60, 0)
#ifdef DX24
		elseif(currentState == ST_WAIT_START)  then
			showHistoric = not showHistoric
			yHistoric = 0
#endif
		end
	end

#ifdef DX24
	if(showHistoric == true) then
		goto done
	end
#endif

   -- On raising hedge of working switch
	if((wrkVal - lastWrkVal) > 0) then
   	if(currentState == ST_WAIT_START) then
      	updateTimeVariables()
        currentState = ST_PREPAR
#ifdef DX24
			if(#historicList > jsnConfig.maxHistoScore) then
				system.messageBox(lang.hsl, 4)
			end
#endif
        goto done
      end
   end

   -- On raising hedge of motor switch
   if((motorVal - lastMotorVal) > 0) then
      if(motorNeverStarted == true) then
			motorTime = 0
			motorNeverStarted = false
			currentState = ST_MOTOR
#ifdef DX24
			motorCount = motorCount + 1
			system.pSave("motorCount", motorCount)
#endif
      end
      
   -- On falling hedge of motor switch
   elseif((motorVal - lastMotorVal) < 0) then
      if(currentState == ST_MOTOR) then
         remainBeforeAlt = 10000
         currentState = ST_BEFORE_ALT
      end
   end

	-- On raising hedge of stop switch
	if((stopVal - lastStopVal) > 0) then
		if(currentState ~= ST_STOP) then
			if(currentState ~= ST_WAIT_START) then
				currentState = ST_STOP
			end
		elseif(currentState == ST_STOP) then
			onStopChrono()
      end
   end

   ::done::

	local currentTime = system.getTimeCounter()
	local currentTimeSec = math.floor(currentTime/1000)

   -- calculate delta time (in msec) from previous loop
   local delta = currentTime - lastTime

	-- Update state accordingly to switches action
   -- Update counters accordingly to state

	if(currentState == ST_PREPAR) then
		if(remainPreparTime <= 0) then
			remainWorkingTime = remainWorkingTime - delta
			currentState = ST_WORK
		else
			remainPreparTime = remainPreparTime - delta
		end
	elseif(currentState == ST_WORK) then
		remainWorkingTime = remainWorkingTime - delta
	elseif(currentState == ST_MOTOR) then
		remainWorkingTime = remainWorkingTime - delta
		flyTime = flyTime + delta
		motorTime = motorTime + delta
		if(motorTime >= 30000) then			-- 30 sec max motor on
			currentState = ST_BEFORE_ALT
		end
	elseif(currentState == ST_BEFORE_ALT) then
		remainWorkingTime = remainWorkingTime - delta
		flyTime = flyTime + delta
		remainBeforeAlt = remainBeforeAlt - delta
		if(remainBeforeAlt <= 0) then
			currentState = ST_FLIGHT
		end
	elseif(currentState == ST_FLIGHT) then
		if(remainWorkingTime >= 0) then
			remainWorkingTime = remainWorkingTime - delta
			flyTime = flyTime + delta

			-- get height from alti device after 'getHeightTime' second
			-- that fix a bug on altiV4
			if(currentAltiSensorID and (flyTime > getHeightTime)) then
				local val = system.getSensorByID(currentAltiSensorID, currentAltiSensorParam)
				altiMotorStop = val.value
			end
		else
			currentState = ST_OVERTIME
		end
	elseif(currentState == ST_OVERTIME) then
		remainWorkingTime = remainWorkingTime - delta
	end

   if((currentTimeSec - lastTimeSec) == 1) then	-- raised only every seconds
      if(jsnConfig) then -- Check if wav file must be played
			local color
			if(currentState == ST_PREPAR) then
				color = startAction(jsnConfig.states.prepar, remainPreparTime)
#ifdef DX24
				if(color) then preparColor = color end
#endif
			elseif(currentState == ST_WORK) then
				color = startAction(jsnConfig.states.work, remainWorkingTime)
#ifdef DX24
				if(color) then workColor = color end
#endif
			elseif(currentState == ST_FLIGHT) then
				startAction(jsnConfig.states.work, remainWorkingTime)
				color = startAction(jsnConfig.states.flight, flyTime)
#ifdef DX24
				if(color) then flightColor = color end
#endif
			elseif(currentState == ST_MOTOR) then
				color = startAction(jsnConfig.states.motor, motorTime)
#ifdef DX24
				if(color) then motorColor = color end
#endif
			elseif(currentState == ST_BEFORE_ALT) then
				color = startAction(jsnConfig.states.alt, remainBeforeAlt)
#ifdef DX24
				if(color) then altColor = color end
#endif
			end
      end
   end

	lastWrkVal = wrkVal
	lastStopVal = stopVal
	lastMotorVal = motorVal
	lastStatusVal = statusVal
	lastTime = currentTime
	lastTimeSec = currentTimeSec
end
 
return {init=mainInit, loop=mainLoop, author="QuarkEmu", version="2.3", name="F5J Chrono"}
