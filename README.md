
#F5J Chrono - v 2.3 - Documentation
## introduction

This LUA application uses 1.3 API included in 4.27 LUA firmware Tx, published by Jeti Model s.r.o.
Any previous Tx versions are not supported.

Developed with QuarkEmu Jeti emulator (http://twilmot.free.fr/QuarkEmu/)

WARNING, this version is not back compatible with app V1. You must save all app V1 files and directory on your Mac (or PC), remove V1 app files and dir from your RadioControl before installing app V2.

## What is new since last version



### Bugs fix
* Due to some memory limitations on DC/DS 16 (causing bad acces to audio sytem sounds, RC freeze or RC rebooting depending of your current model memory foot print), we introduce one dedicated ligther version for DC/DS 16 and another one for DC/DS 24 with full options
* Removed audio messages played twice in the same second, in some race condition
* Fix bad display when remaining work time goes negative

### DC/DS 16 only

* Remove scores history function
* Remove motor count function
* Remove label color function

### DC/DS 16 & DC/DS 24

* Add new OVERTIME state (see states diagram) for future use

### DC/DS 24 only

 * Add custom duration (from 1 min to 10 min) for training

## installation

1. Start your Mac (or PC) and your Jeti Tx, plug USB cable in, then choose USB link mode on your Tx
2. Decompress zip file and move **F5JChrono.lc** (Lua precompiled) and **/F5JChrono** directory from your Mac (or PC) into your **/Apps** Tx directory. You can remove **/doc** to free some space in your Tx
3. Exit your Tx from USB link mode
4. Install application by going to Jeti menu  /Main Menu/Applications/Applications
5. Add F5J Chrono telemetry window

## User interface
F5J Chrono provide 2 kind of screens

### Setting screen
Located in Tx menu /Main menu/Chronos/Sensors/ChronoF5j

![Alt Image Text](http://twilmot.free.fr/QuarkEmu/assets/img/F5J%20Chrono%20setup.png)

Switches | Description
---------|:-------------
| Round type | ***10 min round*** or ***15 min Flyoff*** depending on type of round you currently are
| Prepar. duration default| Use to set default preparation time, between 0 and 120 seconds. This duration is active only if 'Fast prepar. switch' switch/pot (see below) is not assigned
| Time prep. switch | **MUST**^1 be assigned. Rising edge will trigger time of preparation countdown
| Motor switch | **MUST**^1 be assigned. Rising edge will trigger time (counter) of time motor (max of 30 sec). Falling edge will trigger the time (time counter) before measuring the altimeter (an 'alt' message with the remaining time is displayed)
| Stop/reset switch | **MUST**^1 be assigned. The first rising edge will trigger shutdown of the counters flight time and working time. Second rising edge will trigger reset of the app (return to the initial state)
| Fast prepar. switch | **Optional** switch/pot. It allows preselection of one from 4 values ​​of preparation time (10 sec, 30 sec, 1 min and 2 min). These values ​​can be changed in the section 'fastSel' Of the file /Apps/F5JChrono/config.jsn. An interesting option is to assign a potentiometer type non-centered proportional to sweep all pre-settings quickly. You can also make the choice of a inter to 3 positions (in this case one of the preselection will not be available)
| State Switch | **Optional** switch. This command has 2 functions depending on the status of the state machine: - During `ST_WAIT_START` state (Ready to start blinking), it displays the scores history of the last rounds. - During the ST_FLIGHT state, it plays a voice message of the remaining work time
| Height sensor | Optional (prohibited in competition): In training, display altitude value 10 sec after motor off. You must of course choose the name of the appropriate sensor in the list
| Motor start Number ^2 | Value of the number of times motor was started 
| Clear histo after round ^2| Erase all rounds after the displayed round number

^1 F5JChrono will not started until these switches will not be assigned

^2 only DC/DS 24


### Telemetry screen
Located in Tx Main menu

![Wait start](F5JC-tel wait.png "Wait start")

For telemetry screen setup, to go Tx menu /Main menu/Chronos/Sensors/Telemetry displayed. Once added, It displays a synthesis of all chronometers according to states machine managed inside application.

When states machine is in initial state `ST_WAIT_START`, a blinking 'ready to start' message is displayed in the right upper corner. If you have assigned 'prep. fast', move switch/pot. to see different preparation time.


## Files

### Configuration file
Located in /Apps/F5JChrono/config.jsn

~~~~json
{
	"config":
	{
		"version":"1.1",
		"getHeightTime":12,
		"maxHistoScore": 18,
		"states":
		{
			"prepar":
			[
				{"Time":120,"File":"2min.wav"},
				{"Time":60, "File":"1min.wav"},
				{"Time":30, "File":"30sec.wav"},
				{"Time":20, "File":"20sec.wav"},
				{"Time":10, "File":"10.wav", "Color":0},
				{"Time":9,  "File":"9.wav"},
				{"Time":8,  "File":"8.wav"},
				{"Time":7,  "File":"7.wav"},
				{"Time":6,  "File":"6.wav"},
				{"Time":5,  "File":"5.wav"},
				{"Time":4,  "File":"4.wav", "Color":0xD00000},
				{"Time":3,  "File":"3.wav"},
				{"Time":2,  "File":"2.wav"},
				{"Time":1,  "File":"1.wav", "Color":0xFF0000},
				{"Time":0,  "File":"0.wav", "Vib":"R2"}
			],
			"work":
			[
				{"Time":900,"File":"15min.wav"},
				{"Time":840,"File":"14min.wav"},
				{"Time":780,"File":"13min.wav"},
				{"Time":720,"File":"12min.wav"},
				{"Time":660,"File":"11min.wav"},
				{"Time":599,"File":"10min.wav"},
				{"Time":540,"File":"9min.wav"},
				{"Time":480,"File":"8min.wav"},
				{"Time":420,"File":"7min.wav"},
				{"Time":360,"File":"6min.wav"},
				{"Time":300,"File":"5min.wav"},
				{"Time":240,"File":"4min.wav"},
				{"Time":180,"File":"3min.wav"},
				{"Time":120,"File":"2min.wav"},
				{"Time":60, "File":"1min.wav"},
				{"Time":40, "File":"40sec.wav"},
				{"Time":30, "File":"30sec.wav"},
				{"Time":25, "File":"25sec.wav"},
				{"Time":20, "File":"20sec.wav"},
				{"Time":15, "File":"15sec.wav"},
				{"Time":13, "File":"13.wav"},
				{"Time":12, "File":"12.wav"},
				{"Time":11, "File":"11.wav"},
				{"Time":10, "File":"10.wav"},
				{"Time":9,  "File":"9.wav"},
				{"Time":8,  "File":"8.wav"},
				{"Time":7,  "File":"7.wav"},
				{"Time":6,  "File":"6.wav"},
				{"Time":5,  "File":"5.wav"},
				{"Time":4,  "File":"4.wav"},
				{"Time":3,  "File":"3.wav"},
				{"Time":2,  "File":"2.wav"},
				{"Time":1,  "File":"1.wav"},
				{"Time":0,  "Cnt":0, "Freq":5000, "Length":1000}		
			],
			"flight":
			[
			],
			"motor":
			[
				{"Time":30, "File":"30.wav"},
				{"Time":29, "File":"29.wav"},
				{"Time":28, "File":"28.wav"},
				{"Time":27, "File":"27.wav"},
				{"Time":26, "File":"26.wav"},
				{"Time":25, "File":"25.wav"},
				{"Time":24, "File":"24.wav"},
				{"Time":23, "File":"23.wav"},
				{"Time":22, "File":"22.wav"},
				{"Time":21, "File":"21.wav"},
				{"Time":20, "File":"20.wav"},
				{"Time":19, "File":"19.wav"},
				{"Time":18, "File":"18.wav"},
				{"Time":17, "File":"17.wav"},
				{"Time":16, "File":"16.wav"},
				{"Time":15, "File":"15.wav"},
				{"Time":14, "File":"14.wav"},
				{"Time":13, "File":"13.wav"},
				{"Time":12, "File":"12.wav"},
				{"Time":11, "File":"11.wav"},
				{"Time":10, "File":"10.wav"},
				{"Time":9,  "File":"9.wav"},
				{"Time":8,  "File":"8.wav"},
				{"Time":7,  "File":"7.wav"},
				{"Time":6,  "File":"6.wav"},
				{"Time":5,  "File":"5.wav"},
				{"Time":4,  "File":"4.wav"},
				{"Time":3,  "File":"3.wav"},
				{"Time":2,  "File":"2.wav"},
				{"Time":1,  "File":"1.wav"},
				{"Time":0,  "Cnt":0, "Freq":5000, "Length":1000}
			],
			"alt":
			[
				{"Time":10, "File":"10.wav"},
				{"Time":9,  "File":"9.wav"},
				{"Time":8,  "File":"8.wav"},
				{"Time":7,  "File":"7.wav"},
				{"Time":6,  "File":"6.wav"},
				{"Time":5,  "File":"5.wav"},
				{"Time":4,  "File":"4.wav"},
				{"Time":3,  "File":"3.wav"},
				{"Time":2,  "File":"2.wav"},
				{"Time":1,  "File":"1.wav"},
				{"Time":0,  "Cnt":0, "Freq":5000, "Length":1000}		
			]
		},
		"fastSel":
		[
         {"Track":0.2, "Time":180},
         {"Track":0.4, "Time":120},
         {"Track":0.6, "Time":60},
         {"Track":0.8, "Time":30},
         {"Track":1.0, "Time":4}
		]
	}
}
~~~~

Supported JSON Object Type:

| Object        | Fields | Description
----------------|--------|-----------
| config        |        | Consolidates all of the configuration object
|               | version       | Current format version of this file
|               | getHeightTime | Duration (in sec) from state flight start to get a Height measure. (**Height sensor** must be assigned, and altitude reached must be upper than 1 meter)
| | maxHistoScore       | Max number of scores stored. **Warning**: Tx memory must be preserved to a too big number, specially on DC/DS 14/16 tx. When this limit is reached, system message is displayed 4 sec during 'Prepar. time' state and scores are no more saved.
|      |  audioDir      | Specifies an alternate root path of wav files specified by the 'File'
| states        |        | Must be one of the 5 states (**prepar**, **work**, **flight**, **motor**, **alt**) on which actions can be associated. Actions will be triggered upon crossing the value entered in 'Time' field by the corresponding chronometer. Types of actions supported: (**File**, **Cnt** and **Number** are exclusive commands, once a time)
|            | File |     Plays a wav file "located in the /Apps/F5JChrono/audio/xx directory (xx = being the language code supported in locale.jsn. Ex: en, fr ...) or in 'audioDir'
|            | Cnt |     Shows a beep. Ex: "Cnt": 0, "Freq": 5000, "Length": 1000
|            | Number|   Play a voice. Ex: "Number": 10, "Decimals": 0, "Unit": "s", "Label": "Run time"
|            | Color |    (DC/DS 24 only) Changes the color of chronometer label associated with the current state
|            | Vib   |     (DC/DS 24 only) Start vibration defined stick. Ex: "Vib":"L2" means vibration on left stick (L), vibration profil 2 (see system.vibration() function of JETI doc DC_DS_Lua_API_V1.2.pdf)
| fastSel    |       | Quick selection ladder definition
|            | Track | Value [0, 1] proportionnal to switch/pot curse assigned to **Fast prepar. switch**
|            | Time  | Duration (in sec) associate to a Track value switch/pot curse


### Rounds results history file (only DC/DS 24)
Located in /Apps/F5JChrono/histo.jsn

Scores are stored into a JSON array, 1 line is 1 round score recorded after front edge of Stop/reset switch ON, during `STOP` state. This file can be deleted or copy without damaging Chrono state process.

~~~~json
[
 "01        00:02.0      00:00.9      000",
 "02        00:01.8      00:01.8      000",
 "03        00:02.5      00:01.0      000"
]

~~~~

### Translation file
Located in /Apps/F5JChrono/locale.jsn

You can add you own translation section in this file, accordingly to country code returned by system.getLocale() Lua function of your Tx.

~~~~json

{
   "default":"en",

   "en":{
      "an":"F5J Chrono",
      "rdl":"Round type",
      "rrl":"10 min round",
      "frl":"15 min Flyoff",
      "trl":"Training",
      "twt":"Training work time",
      "pfdl":"Prepar. duration default",
      "wtsl":"Prepar. time switch",
      "srsl":"Stop/reset switch",
      "msl":"Motor switch",
      "ssl":"Status switch",
      "fpsl":"Fast prepar. switch",
      "r2sl":"Ready to start",
      "wl":"Work",
      "fl":"Flight",
      "ml":"Motor",
      "mcl":"Motor start count",
      "mclt":"Mot #%.1d",
      "sl":"Stopped",
      "al":"Height sensor",
      "chl":"Clear histo after round",
      "hhl":"Round   Fly time   Mot time   Hgt",
      "hsl":"Histo score list too high",
      "ovt":"Overtime"
   },

  "fr":{
		"an":"Chrono F5J",
      "rdl":"Type de manche",
		"rrl":"Manche 10 min",
		"frl":"Flyoff 15 min",
		"trl":"Entrainement",
		"twt":"Durée entrain.",
		"pfdl":"Durée prép. défaut",
		"wtsl":"Inter temps prép.",
		"srsl":"Inter stop/r.a.z",
		"msl":"Inter moteur",
		"ssl":"Inter d'état",
		"fpsl":"Inter prép. rapide",
		"r2sl":"Prêt à demarrer",
		"wl":"Travail",
		"fl":"Vol",
		"ml":"Moteur",
      "mcl":"Nb démar. moteur",
		"mclt":"Mot(%.1d)",
      "sl":"Stoppé",
      "al":"Capteur Altitude",
      "chl":"Raz histo apres manche",
      "hhl":"Manche   Tmp vol   Tmp mot   Alt",
      "hsl":"Liste histo score trop grande",
      "ovt":"Tmp depassé"
  }
}


~~~~

## Inner automaton states diagram

Initial state is double circle.

![Automaton states diagram](F5JC-states.png "Automaton states diagram")

^* Only DC/DS 24

States list

|State         | Comment                        | Image
|--------------|:-------------------------------|------------------------------------------------
| `WAIT START` |  'ready to start' is blinking  | ![Wait start](F5JC-tel wait.png "Wait start")
| `SHOW SCORES` (only DC/DS 24)|  Show round scores history     | ![Scores history](F5JC-tel histo.png "Scores history")
| `PREPAR`     |  Countdown preparation time    | ![Wait start](F5JC-tel prep.png "Wait start")
| `WORK`       |  Countdown work time           | ![Work](F5JC-tel work.png "Work")
| `MOTOR`      |  Countdown motor time          | ![Motor](F5JC-tel motor.png "Motor")
| `BEFORE ALT` |  Countdown 10 sec before altitude recording (Alt xx) | ![Alt](F5JC-tel alt.png "Alt")
| `FLIGHT`     |  Count flight time (display height 'H xxx m' if Height sensor has been assigned | ![Flight](F5JC-tel flight.png "Flight") ![Height](F5JC-tel height.png "Height")
| `STOP`       |  Stops all timers and append new score line into history  | ![Stop](F5JC-tel stop.png "Stop")
| `OVERTIME`   |  Negative remaining work time | ![Stop](F5JC-tel over.png "Stop")
| `RESET`      |  Reset all timers then go to `WAIT STATUS` state | no screen


## FAQ

> * **Question**: Telemetry screen shows preparation time initial value, but Prepar. time switch does not make any countdown start
> * **Anwser**:  In Chronos/Sensors/Chrono F5J menu, you have to assign at least the first three commands (preparation time, motor and stop/reset), otherwise F5J Chrono will never start.


## Versions history

Version | Description
----------|:----------------------------------------------------|
| 2.3     | Add custom training duration (DC 24 only)
|         | Split into ligth (DC/DS 16) version and a full one (DC/DS 24)
| 2.2     | Private version, Lua code refactoring to make 16/24 code splitable
| 2.1     | Passing counters to the tenth of a second 
|         | Display altitude value after motor off (for | training only)
|         | Add vibration action (DS/DC 24 only) in config.jsn
|         | Altitude reached time is configurable (see getHeightTime)
|         | Max scores limit is configurable (see maxHistoScore)
| 2.0     | Refactoring of config.jsn file format -> Memory optimization, 7% off
|         | Add number' and 'color' (DS/DC 24) new action type in config.jsn
|         | Added audioDir in config.jsn
| 1.0     | Initial version


Copyright (c) 2017-2022 - Bertrand & Thierry WILMOT - QuarkEmu
